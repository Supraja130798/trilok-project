FROM tomcat
WORKDIR webapps/
COPY target/project1-1.0.0.RELEASE.jar .
RUN ls -lrt 
RUN chmod 777 project1-1.0.0.RELEASE.jar
EXPOSE 8080
CMD ["java", "-jar", "project1-1.0.0.RELEASE.jar"]